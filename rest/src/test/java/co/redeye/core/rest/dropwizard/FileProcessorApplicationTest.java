package co.redeye.core.rest.dropwizard;

import io.dropwizard.setup.Bootstrap;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Cool Beans
 * Created by george on 13/09/15.
 */
public class FileProcessorApplicationTest {


    @Test
    public void testGetName() throws Exception {
        FileProcessorApplication fileProcessorApplication = new FileProcessorApplication();
        assertEquals("file-processor", fileProcessorApplication.getName());
    }

    @Test
    public void testInitialize() throws Exception {
        FileProcessorApplication fileProcessorApplication = new FileProcessorApplication();
        Bootstrap<FileProcessorConfiguration> bootstrap = new Bootstrap<>(fileProcessorApplication);
        fileProcessorApplication.initialize(bootstrap);
    }

    @Test
    public void testRun() throws Exception {
        FileProcessorApplication fileProcessorApplication = new FileProcessorApplication();
        fileProcessorApplication.run("server", "uber.yml");
    }

    // todo: look way to stop server after test.
//    @Test
//    public void testMain() throws Exception {
//        String[] args = {"server", "file-processor.yml"};
//        FileProcessorApplication.main(args);
//    }
}