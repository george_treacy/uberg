package co.redeye.core.rest.dropwizard.resource;


import co.redeye.core.dagger.S3DocumentModule;
import co.redeye.core.service.DocumentProcessor;
import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.objects.S3DocumentRequest;
import co.redeye.core.service.objects.MetaData;
import com.codahale.metrics.annotation.Timed;
import dagger.Component;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 * Created on 4/09/2015.
 */
@Path("/file-processor")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class FileProcessorResource {

    public FileProcessorResource() {
    }

    @Singleton
    @Component(modules = {S3DocumentModule.class})
    public interface DocumentComponent {
        DocumentProcessor processor();
    }

    /**
     * @param documentRequest
     * @return
     * @throws DocumentProcessorException
     */
    @POST
    @Timed
    public MetaData getMetaData(S3DocumentRequest documentRequest) throws DocumentProcessorException {
        DocumentComponent documentComponent = DaggerFileProcessorResource_DocumentComponent.create();
        return documentComponent.processor().process(documentRequest);
    }
}
