package co.redeye.core.rest.dropwizard;

import co.redeye.core.rest.dropwizard.resource.FileProcessorResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created on 4/09/2015.
 */
class FileProcessorApplication extends Application<FileProcessorConfiguration> {

    private static final Logger logger = LoggerFactory.getLogger(FileProcessorApplication.class);

    public static void main(String[] args) throws Exception {
        logger.debug("In main.");
        new FileProcessorApplication().run(args);
    }

    @Override
    public String getName() {
        return "file-processor";
    }

    @Override
    public void initialize(Bootstrap<FileProcessorConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(FileProcessorConfiguration configuration,
                    Environment environment) {
        final FileProcessorResource resource = new FileProcessorResource();
        final TemplateHealthCheck healthCheck =
                new TemplateHealthCheck(configuration.getTemplate());
        environment.healthChecks().register("template", healthCheck);
        environment.jersey().register(resource);
    }

}
