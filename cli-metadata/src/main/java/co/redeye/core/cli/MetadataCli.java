package co.redeye.core.cli;

import co.redeye.core.dagger.LocalDocumentModule;
import co.redeye.core.dagger.S3DocumentModule;
import co.redeye.core.service.DocumentProcessor;
import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.filer.S3Utils;
import co.redeye.core.service.objects.MetaData;
import co.redeye.core.service.objects.S3DocumentRequest;
import com.google.gson.Gson;
import dagger.Component;
import org.apache.commons.cli.*;

import javax.inject.Singleton;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by george on 15/09/15.
 */
class MetadataCli {

    private CommandLine cmd;
    private final HelpFormatter formatter;
    private final Options options;
    private final Gson gson;
    private final CommandLineParser parser;

    private MetadataCli() {
        this.gson = new Gson();
        this.options = new Options();
        this.formatter = new HelpFormatter();
        this.parser = new DefaultParser();
    }

    public static void main(String[] args) {
        MetadataCli cli = new MetadataCli();
        cli.run(args);
    }

    private void run(final String[] args) {
        options.addOption("s", true, "process json from string");
        options.addOption("f", true, "process json from file");
        options.addOption("l", true, "process local file file");
        options.addOption("j", false, "output to json");
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            doExit();
        }
        processSwitches();
    }

    private void processSwitches() {
        String jsonString = null;
        if (cmd.hasOption("s")) {
            jsonString = cmd.getOptionValue("s");
            handleS3(jsonString);
        } else if (cmd.hasOption("f")) {
            String filePath = cmd.getOptionValue("f");
            if (filePath != null) {
                try (FileInputStream in = new FileInputStream(filePath)) {
                    jsonString = S3Utils.getStringFromStream(in);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            handleS3(jsonString);
        } else if (cmd.hasOption("l")) {
            handleLocal();
        } else {
            doExit();
        }
    }

    private void handleS3(final String jsonString) {
        S3DocumentRequest request;
        request = gson.fromJson(jsonString, S3DocumentRequest.class);
        S3DocumentComponent documentComponent = DaggerMetadataCli_S3DocumentComponent.create();
        try {
            MetaData metaData = documentComponent.processor().process(request);
            processOutput(metaData);
        } catch (DocumentProcessorException e) {
            e.printStackTrace();
        }
    }

    private void handleLocal() {
        S3DocumentRequest request;
        final String filePath = cmd.getOptionValue("l");
        if (filePath != null) {
            request = new S3DocumentRequest();
            request.setFilename(filePath);
            LocalDocumentComponent documentComponent = DaggerMetadataCli_LocalDocumentComponent.create();
            MetaData metaData = null;
            try {
                metaData = documentComponent.processor().process(request);
            } catch (DocumentProcessorException e) {
                e.printStackTrace();
            }
            processOutput(metaData);
        } else {
            doExit();
        }
    }

    private void processOutput(final MetaData metaData) {
        if (cmd.hasOption("j")) {
            System.out.println(gson.toJson(metaData));
        } else {
            for (String key : metaData.getDocumentProperties().keySet()) {
                System.out.println(key + " " + metaData.getDocumentProperties().get(key));
            }
        }
    }

    private void doExit() {
        formatter.printHelp("uber-cli-metadata", options);
        System.exit(1);
    }

    @Singleton
    @Component(modules = {LocalDocumentModule.class})
    public interface LocalDocumentComponent {
        DocumentProcessor processor();
    }

    @Singleton
    @Component(modules = {S3DocumentModule.class})
    public interface S3DocumentComponent {
        DocumentProcessor processor();
    }

}
