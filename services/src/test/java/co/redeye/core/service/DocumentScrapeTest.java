package co.redeye.core.service;

import co.redeye.core.service.objects.MetaData;
import co.redeye.core.service.objects.S3DocumentRequest;
import co.redeye.core.service.parser.DocumentScrape;
import junit.framework.TestCase;

/**
 * Created on 14/09/2015.
 */
public class DocumentScrapeTest extends TestCase {

    private static final String filePath = "/tmp/test.doc";

    public void testProcess() throws Exception {
        S3DocumentRequest request = new S3DocumentRequest();
        DocumentScrape scrape = new DocumentScrape();
        MetaData metaData = scrape.process(filePath, request);
        for (String key : metaData.getDocumentProperties().keySet()) {
            System.out.println(key + " " + metaData.getDocumentProperties().get(key));
        }
    }
}