package co.redeye.core.service.filer;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.objects.DocumentRequest;
import co.redeye.core.service.objects.MetaData;
import co.redeye.core.service.objects.S3DocumentRequest;
import co.redeye.core.service.parser.DefaultParser;
import co.redeye.core.service.parser.Taxonomy;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Cool Beans
 * Created by george on 17/09/15.
 */
public class S3FilerTest {

    final String bucket = "georgeit";
    final String key = "arow";

    @Ignore
    @Test
    public void testGetListOfFiles() throws Exception {
        S3Filer filer = new S3Filer();
        ObjectListing listing = filer.getListOfFiles(bucket, key);
        List<S3ObjectSummary> list = listing.getObjectSummaries();
        for (S3ObjectSummary summary : list) {
            System.out.println("key: " + summary.getKey() + " etag: " + summary.getETag());
            S3DocumentRequest request = new S3DocumentRequest();
            request.setBucket(summary.getBucketName());
            request.setPrefix(summary.getKey());

            String tempFilePath = filer.getLocalFileHandle(request);
            DefaultParser parser = new DefaultParser();
            MetaData metaData = parser.getMetaData(tempFilePath, request);
            try {
                metaData.setMime((new Taxonomy().getFileTypeByFile(tempFilePath)));
            } catch (IOException e) {
                throw new DocumentProcessorException(e);
            }

            System.out.println(metaData.getDocumentProperties().toString());

            filer.deleteLocalFile(tempFilePath);
        }
    }

    @Test
    public void testGetListOfFiles2() throws Exception {
        S3Filer filer = new S3Filer();
        filer.getListOfFiles(bucket);
    }
}