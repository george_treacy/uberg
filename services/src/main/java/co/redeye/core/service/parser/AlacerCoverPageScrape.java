package co.redeye.core.service.parser;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.filer.S3Utils;
import co.redeye.core.service.objects.MetaData;
import co.redeye.core.service.objects.Revision;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Created on 31/08/2015.
 */
class AlacerCoverPageScrape extends DocumentScrape {

    private static final Logger logger = LoggerFactory.getLogger(AlacerCoverPageScrape.class);

    private static final String fileTypeDoc = "application/msword";
    private static final String fileTypeDocx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

    /**
     * Parse a text file, line by line.
     * If line contains a colon : then assume key to the left and
     * value to the right.
     * <p>
     * e.g.
     * Name : George
     * <p>
     * "Name" -> "George"
     */
    public MetaData parseAlacer(final String tempFileName, MetaData metadata, final String plainText) throws DocumentProcessorException {
        Taxonomy taxonomy = new Taxonomy();
        String fileType = null;
        try {
            fileType = taxonomy.getFileTypeByFile(tempFileName);
        } catch (IOException e) {
            S3Utils.handleException(e);
        }
        if (fileType != null) {
            switch (fileType) {
                case fileTypeDoc:
                    metadata = parseDoc(metadata, plainText);
                    break;
                case fileTypeDocx:
                    metadata = parseDocx(metadata, plainText);
                    break;
            }
        }
        return metadata;
    }

    private MetaData parseDoc(MetaData metaData, final String plainText) throws DocumentProcessorException {
        String[] lines = plainText.split("\n\n");
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i].trim();
            if (line.length() > 0) {
                String[] bits = line.split("\n");
                if (bits.length == 2) { // must be key value with :
                    String key = bits[0].trim();
                    String value = bits[1].trim();
                    if (value.contains("\\*")) {
                        value = value.split(" \\\\")[0];
                    }
                    metaData.getEmbeddedAttributes().put(key, value);
                } else if (bits.length == 3) { // must be a revision record
                    String number = bits[0].trim();
                    String date = bits[1].trim();
                    String comments = bits[2].trim();
                    if (number.charAt(0) != 8194) {
                        Revision revision = new Revision(number, date, comments);
                        metaData.getRevisions().add(revision);
                    }
                }
            }
        }
        return metaData;
    }

    private MetaData parseDocx(MetaData metadata, final String plainText) throws DocumentProcessorException {
        String[] lines = plainText.split("\n\n");
        for (int i = 0; i < lines.length; i++) {
            String line = lines[i].trim();
            if (line.trim().length() > 0) {
                String[] bits = line.split("\n\t");
                if (bits.length == 2) { // must be key value with :
                    metadata.getEmbeddedAttributes().put(bits[0].trim(), bits[1].trim());
                } else if (bits.length == 3) { // must be a revision record
                    String number = removeNonPrinting(bits[0].trim());
                    String date = removeNonPrinting(bits[1].trim());
                    String comments = removeNonPrinting(bits[2].trim());
                    if (number.length() > 0 && date.length() > 0 && comments.length() > 0) {
                        Revision revision = new Revision(number, date, comments);
                        metadata.getRevisions().add(revision);
                    }
                }
            }
        }
        return metadata;
    }

    private String removeNonPrinting(final String line) {
        return line.replaceAll("[^\\x20-\\x7E]", "");
    }

    private void dumpRevisions(List<Revision> revisions) {
        for (Revision revision : revisions) {
            System.out.println(revision.number + ", " + revision.date + ", " + revision.comments);
        }
    }

    private void dumpAttributes(Map<String, String> attributeMap) {
        for (Map.Entry<String, String> entry : attributeMap.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.println(key + " => " + value);
        }
    }

}
