package co.redeye.core.service.objects;

/**
 * Created on 2/09/2015.
 */
public class S3DocumentRequest implements DocumentRequest {

    private String bucket;
    private String prefix;
    private String filename;
    private String region;
    private String company;

    public S3DocumentRequest() {
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
