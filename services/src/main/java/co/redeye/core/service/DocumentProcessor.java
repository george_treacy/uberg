package co.redeye.core.service;

import co.redeye.core.service.filer.Filer;
import co.redeye.core.service.objects.DocumentRequest;
import co.redeye.core.service.objects.MetaData;
import co.redeye.core.service.parser.Parser;
import co.redeye.core.service.parser.Taxonomy;

import javax.inject.Inject;
import java.io.IOException;

public class DocumentProcessor {
    private final Filer filer;
    private final Parser parser;

    @Inject
    DocumentProcessor(Filer filer, Parser parser) {
        this.filer = filer;
        this.parser = parser;
    }

    public MetaData process(final DocumentRequest request) throws DocumentProcessorException {
        String tempFilePath = filer.getLocalFileHandle(request);
        MetaData metaData = parser.getMetaData(tempFilePath, request);
        try {
            metaData.setMime((new Taxonomy().getFileTypeByFile(tempFilePath)));
        } catch (IOException e) {
            throw new DocumentProcessorException(e);
        }
        filer.deleteLocalFile(tempFilePath);
        return metaData;
    }
}
