package co.redeye.core.service.objects;

/**
 * Created on 2/09/2015.
 */
public interface DocumentRequest {
    String getPrefix();
    String getFilename();
}
