package co.redeye.core.service.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created on 3/09/2015.
 */
public class MetaData {
    private String pathName;
    private String company;
    private String mime;

    private Map<String, String> documentProperties;
    private Map<String, String> embeddedAttributes;
    private List<Revision> revisions;

    public MetaData() {
        documentProperties = new HashMap<>();
        embeddedAttributes = new HashMap<>();
        revisions = new ArrayList<>();
    }

    public MetaData(String pathName, String company) {
        this();
        this.pathName = pathName;
        this.company = company;
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Map<String, String> getDocumentProperties() {
        return documentProperties;
    }

    public void setDocumentProperties(Map<String, String> documentProperties) {
        this.documentProperties = documentProperties;
    }

    public Map<String, String> getEmbeddedAttributes() {
        return embeddedAttributes;
    }

    public void setEmbeddedAttributes(Map<String, String> embeddedAttributes) {
        this.embeddedAttributes = embeddedAttributes;
    }

    public List<Revision> getRevisions() {
        return revisions;
    }

    public void setRevisions(List<Revision> revisions) {
        this.revisions = revisions;
    }

    public String getMime() {
        return mime;
    }

    public void setMime(String mime) {
        this.mime = mime;
    }
}
