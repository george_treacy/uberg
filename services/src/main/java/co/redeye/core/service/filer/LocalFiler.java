package co.redeye.core.service.filer;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.objects.DocumentRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LocalFiler implements Filer {

    private static final Logger logger = LoggerFactory.getLogger(LocalFiler.class);

    public LocalFiler() {
    }

    @Override
    public String getLocalFileHandle(DocumentRequest request) throws DocumentProcessorException {
        return request.getPrefix() != null && !"".equals(request.getPrefix())
                ? request.getPrefix() + "/" + request.getFilename()
                : request.getFilename();
    }

    @Override
    public void deleteLocalFile(String filePath) throws DocumentProcessorException {
        // do nothing.
    }
}
