package co.redeye.core.service.objects;

/**
 * Created on 3/09/2015.
 */
public class Revision {
    public String number;
    public String date;
    public String comments;

    public Revision() {
    }

    public Revision(String number, String date, String comments) {
        this.number = number;
        this.date = date;
        this.comments = comments;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
