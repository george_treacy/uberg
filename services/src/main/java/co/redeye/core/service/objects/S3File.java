package co.redeye.core.service.objects;

/**
 * Cool Beans
 * Created by george on 17/09/15.
 */
public class S3File {

    private String bucket;
    private String key;
    private String fileName;

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
