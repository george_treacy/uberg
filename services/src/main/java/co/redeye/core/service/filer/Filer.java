package co.redeye.core.service.filer;

import co.redeye.core.service.DocumentProcessorException;
import co.redeye.core.service.objects.DocumentRequest;

public interface Filer {
    String getLocalFileHandle(final DocumentRequest request) throws DocumentProcessorException;

    void deleteLocalFile(final String filePath) throws DocumentProcessorException;
}
