package co.redeye.core.service.parser;

import org.apache.tika.Tika;

import java.io.File;
import java.io.IOException;

/**
 * Created on 4/09/2015.
 */
public class Taxonomy {

    private final Tika tika;

    public Taxonomy() {
        this.tika = new Tika();
    }

    public String getFileTypeByFile(final String filePath) throws IOException {
        return tika.detect(new File(filePath));
    }

    public String getFileTypeByName(final String fileName) {
        return tika.detect(fileName);
    }

}
