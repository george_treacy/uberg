package co.redeye.core.dagger;

import co.redeye.core.service.filer.Filer;
import co.redeye.core.service.filer.LocalFiler;
import co.redeye.core.service.parser.DefaultParser;
import co.redeye.core.service.parser.Parser;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;


@Module()
public class LocalDocumentModule {
    @Provides
    @Singleton
    Filer provideFiler() {
        return new LocalFiler();
    }

    @Provides
    @Singleton
    Parser provideParser() {
        return new DefaultParser();
    }
}
